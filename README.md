# Example how to use helmfile with git repo target

## Usage

1. create test cluster

```sh
k3d cluster create test
```

2. run helmfile

```sh
helmfile apply
```
